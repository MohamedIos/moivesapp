//
//  MoviesTableViewCell.swift
//  MoviesApps
//
//  Created by Mohamed on 27/02/2018.
//  Copyright © 2018 Mohamed. All rights reserved.
//

import UIKit

class MovieTableViewCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var overview: UILabel!

    @IBOutlet weak var movieImageIndecator: UIActivityIndicatorView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
