//
//  Movies.swift
//  MoviesApps
//
//  Created by Mohamed on 27/02/2018.
//  Copyright © 2018 Mohamed. All rights reserved.
//

import Foundation
import UIKit

class Movies: NSObject, NSCoding  {
   
    
    
    let title: String
    let overview: String
    let imageUrl: String
    let rating: NSNumber
    
     init(title: String, overview: String, imageUrl: String, rating: NSNumber) {
        self.title = title
        self.overview = overview
        self.imageUrl = imageUrl
    self.rating = rating

    }
    
    required init (coder aDecoder: NSCoder) {
        self.title = aDecoder.decodeObject(forKey: "title") as! String
        self.overview = aDecoder.decodeObject(forKey: "overview") as! String
        self.imageUrl = aDecoder.decodeObject(forKey: "imageUrl") as! String
        self.rating = aDecoder.decodeObject(forKey: "rating") as! NSNumber
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(title, forKey: "title")
        aCoder.encode(overview, forKey: "overview")
        aCoder.encode(imageUrl, forKey: "imageUrl")
        aCoder.encode(rating, forKey: "rating")

    }
}
