//
//  NotificationRepresentable.swift
//
//
//  Created by Mohamed on 27/02/2018.
//  Copyright © 2018 Mohamed. All rights reserved.
//

import Foundation

///The conforming type provide Notification.Name representation
protocol NotificationNameRepresentable {
    
    var name: Notification.Name { get }
}

extension NotificationNameRepresentable where Self: RawRepresentable, Self.RawValue == String {
    
    var name: Notification.Name {
        
        let value = String(reflecting: type(of: self)) + "." + self.rawValue
        return .init(rawValue: value)
    }
}

