//
//  GlobalConstants.swift
//  MoviesApps
//
//  Created by Mohamed on 27/02/2018.
//  Copyright © 2018 Mohamed. All rights reserved.
//

import Foundation

//Api key
public let ApiKey = "449d682523802e0ca4f8b06d8dcf629c"

public let MovieURL = "http://api.themoviedb.org/3/movie/top_rated?api_key=449d682523802e0ca4f8b06d8dcf629c&language=en"


public let ImageURL = "https://image.tmdb.org/t/p/w500/"
